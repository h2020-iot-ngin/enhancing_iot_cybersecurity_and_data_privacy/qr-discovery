## QR Discovery

This is the QR Discovery component of the IoT-NGIN project. The QR discovery consists of two modules: the QR-code encoding and decoding module and the GS1 datalink resolver module. The modules can be used independent of each other.

The QR-encode-decode module consists of two components, i.e generation of QR code and verification of QR code. The GS1 key code and key value is stored in QR code. This QR-code can be read to get the GS1 key code and key value which can be used to fetch GS1 datalink resolver entries.

The GS1-datalink resolver module takes in globally uniquely identifier (GS1 key code and Key value) and intelligently redirects the user to the web address based on data and the context of the request. The module consists of three major components:

- data-entry API: A RESTful API which provides data entry service for the resolver and stores the resolver entries into in the SQL server.

- Resolver: The resolver relies on lookup data provided by a mongoDB database. It receives incoming HTTP requests conforming to the GS1 datalink standard and provides a redirect to a web location.

- Build: This component periodically reads any updates from the SQL Server database and updates the MongoDB database for fast lookups.

### Components

   <img src="Project-Architecture.png" alt="Project-Architecture" width="690" height="480">

<table border="1">
<tr><th>Folder Name</th><th>Project</th></tr>
<tr><td>QR code Generator</td><td> The QRcode Generator service creates QR code for GS1datalink key and keycode</td></tr>
<tr><td>QR code Verifier</td><td> The QR code Verifier service reads QR code and gets GS1datalink key and keycode. The GS1datalink key and keycode is used to search the resolver entries </td></tr>
<tr><td>resolver_data_entry_server</td><td>The Data Entry service <b>dataentry-web-server</b> consisting of an API that provides controlled access to Create, Read, Update and Delete (CRUD) operations on resolver records, along with a web-based example user interface that allows easy data entry of this information (and uses the API to perform its operations). 
This project uses a SQL Server database to store information</td></tr>
<tr><td>build_sync_server</td><td>This service runs a 'Build' process once a minute (configurable in Dockerfile) that takes any changes to the data in the SQL database and builds a document for each GS1 key and value that will be stored in MongoDB database. </td></tr>
<tr><td>resolver_web_server</td><td>The resolving service <b>resolver-web-server</b> is written in Node.js for improved performance and scalability which can be used by client applications that supply a GS1 key and value according to the GS1 Digital Link standard. This service performs a high-speed lookup of the specified GS1 key and value, and returns the appropriate redirection where possible.</td></tr>
<tr><td>resolver_sql_server</td><td>The SQL database service <b>dataentry-sql-server</b> uses the SQL Server 2017 Express edition (free to use but with 10GB limit) to provide a stable data storage service for the resolver's data-entry needs.</td></tr>
<tr><td>resolver_mongo_server</td><td>The <b>resolver-mongo-server</b> service hosts MongoDB database which is used by the resolver to have fast lookups. This design allows Resolver to make lightning decisions as all the data for the GS1 Key code and Key value is stored in a singe document. </td></tr>
<tr><td>dashboard_sync_server</td><td>This service runs a statistical process once a 120 seconds (configurable in Dockerfile) that builds up a data graph of the information saved in its database for display by authorised users</td></tr>
<tr><td>frontend_proxy_server</td><td>The frontend web server routing traffic securely to the other containers. Using NGINX, this server's config can be adjusted to support load balancing and more,</td></tr>
<tr><td>digitallink_toolkit_server</td><td>A library server available to all the other container applications that tests incoming data against the official reference implementation of the GS1 Digital Link standard. There is also external access capability though the /dltoolkit API path for clients wishing to test their GS1 Digital Link syntax, and also to compress a GS1 Digital Link URL.</td></tr>
</table>

## Usage

On the main screen of the QR encoding application, there are three operations that could be performed.

1. Create GS1 datalink resolver entry.

- <img src="create-entry.png" alt="create-entry" width="600" height="520">

- The default value of _authentication key_ of administrator is `5555555555555`. If you want to add new account for administrator, run the `python_admin_clients/accounts.py` script to perform CRUD operations on the admin accounts.
- The _Identification Key_ field can be 8, 12, 13, 14, 17, 18 digits. The last digit of all fixed-length, numeric GS1 Identification Keys is a check digit that ensures the integrity of the key. The check digit is calculated using a simple algorithm, based on the other numbers in the key.
- _Linktype_ is the type of link from the GS1 web vocabulary - “https://gs1.org/voc/pip”. The _Link Type_ entry is `gs1:did` for our application. _Iana_language_ is the 2-character language identifier – “en”, “fr”
- _context_ field contains the context of the request such as territory “GB”, “US”.
- _link_title_ is the title describing the destination link.
- _Target URL_ is the target web address which Resolver will redirect you.

### How to calculate a check digit?

- <img src="check-digit.png" alt="check-digit" width="600" height="500">

reference:
https://www.gs1.org/services/check-digit-calculator

2. Generate QR code for GS1 digital link using Identification key type and Identification key value.

   <img src="QR-code-Generator.png" alt="QR-code-Generator" width="600" height="300">

The QR-code will be stored in `QR-encode-decode-gs1datalink/QR-codes` folder.

3. Verify the QR code and search for Resolver entries with Identification KeyType and Key value pair.
   <img src="Decode-QRcode.png" alt="Decode-QRcode" width="600" height="420">

After entering Identification key value, the QR code image will be searched in `QR-encode-decode-gs1datalink/QR-codes` folder and target URLs will be displayed on the screen in tabular format. There can be multiple resolver target URLs for a given Identification Key type and Identification Key value pair.

<hr />
