# GS1 Digital Link Resolver

## Installation Guide

#### Follow these instructions to get GS1 Resolver up and running on your machine complete with some example data.

1. Install the Docker system on your computer. Head to https://www.docker.com/products/docker-desktop for install
   details for Windows and Mac. If you are using Ubuntu Linux, follow install instructions
   here: https://docs.docker.com/install/linux/docker-ce/ubuntu/ - Note that if you wish to install on Windows Server
   editions, read the section on Windows Server at the foot of this README file.
1. <i><b>git clone</b></i> this repository onto your computer. <pre>https://github.com/gs1/GS1_DigitalLink_Resolver_CE.git
1. Open a terminal prompt and change directory to the one at the 'root' of your local cloned copy of this repository, so you can see the file <b>docker-compose.yml</b> in the current folder.
1. List the docker-compose.yml by typing this command:<pre>docker-compose config</pre>
   and then check status of Docker by type this command <pre>docker info</pre>

1. Build the complete end-to-end GS1 Resolver service: <pre>docker-compose build</pre>

1. Make sure you have no SQL Server service (port 1433), MongoDB service (port 27017) or web server (port 80) running on your computer as they will clash with Docker as it tries to start the containers.

1. Run docker-compose with the 'up' command to get Docker to spin up the complete end-to-end application:<pre>docker-compose up -d</pre>(the -d means 'disconnect' - docker-compose will start up everything then hand control back to you).

1. Now wait 10 seconds while the system settles down (the SQL Server service takes a few seconds to initialise when '
   new') then, if you have SQL Server Management Studio installed, go to Step 9, else copy and paste this command which
   will cause you to enter the container and access its terminal prompt:<pre>docker exec -it resolver-sql-server bash</pre>
   Now run this command which will create the database and some example data:<pre>/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P its@SECR3T! -i /gs1resolver_sql_scripts/sqldb_create_script.sql </pre>
   You will see a messages such as '(1 rows affected)' and a sentences that starts 'The module 'END_OF_DAY' depends on
   the missing object...'. These are all fine - the latter messages are shown because some stored procedures are created
   by the SQL script before others - and some stored procedures depend on others not created yet as their creation
   occurs further down this SQL script. As long as the final line says 'Database Create Script Completed' all is well!
   Exit the container with the command:<pre>exit</pre>
1. ALTERNATIVELY to Step 8, if you have Windows and SQL Server Management Studio
   installed (<a href="https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15">download SSMS from here</a>), use it to connect to 'localhost' server, with username 'sa' and password 'its@SECR3T!'.
   Then, click on the 'New Query' button, and copy & paste the contents of 'sqldb_create_script.sql' in the '
   resolver_sql_server' folder into the Query window. Click 'Execute' and the script will run. Then, right-click '
   Databases' in the Object Explorer window and click 'Refresh' and you can see the new database '
   gs1-resolver-ce-v2-1-db'.
1. Head to http://localhost/ui and select the Download page.
1. In the authorization key box, type: "5555555555555" and click the Download button. Save the file to your local
   computer.
1. Click the link to go back to the home page, then choose the Upload page.
1. Type in your authorization key (5555555555555), then choose the file you just downloaded. The Upload page detects '
   Download' -format file and will set all the columns correctly for you. Have look at the example data in each column
   and what it means (read the final section of the PDF document for more details about these columns).
1. Click 'Check file' followed by 'Upload file'.
1. By now the local Mongo database should be built (a build event occurs every one minute) so try out this request in a
   terminal window: <pre> curl -I http://localhost/gtin/09506000134376?serialnumber=12345 </pre> which should result in
   this appearing in your terminal window:

<pre>
HTTP/1.1 307 Temporary Redirect
Server: nginx/1.19.0
Date: Mon, 09 Nov 2020 16:42:51 GMT
Connection: keep-alive
Vary: Accept-Encoding
Access-Control-Allow-Origin: *
Access-Control-Allow-Methods: HEAD, GET, OPTIONS
Access-Control-Expose-Headers: Link, Content-Length
Cache-Control: max-age=0, no-cache, no-store, must-revalidate
X-Resolver-ProcessTimeMS: 9
Link: &#60;https://dalgiardino.com/medicinal-compound/pil.html>; rel="gs1:epil"; type="text/html"; hreflang="en"; title="Product Information Page", &#60;https://dalgiardino.com/medicinal-compound/>;
rel="gs1:pip"; type="text/html"; hreflang="en"; title="Product Information Page", &#60;https://dalgiardino.com/medicinal-compound/index.html.ja>; rel="gs1:pip"; type="text/htm
l"; hreflang="ja"; title="Product Information Page", &#60;https://id.gs1.org/01/09506000134376>; rel="owl:sameAs"
Location: https://dalgiardino.com/medicinal-compound/?serialnumber=12345
</pre>

This demonstrates that Resolver has found an entry for GTIN 09506000134376 and is redirecting you to the website shown
in the 'Location' header. You can also see this in action if you use the same web address. In your web browser, you
should end up at Dal Giordano website. The rest of the information above reveals all the alternative links available for
this product depending on the context in which Resolver was called.

In this example, try changing the serial number - you will see it change in the resulting 'Location:' header, too! This
is an example of using 'URI template variables' to forward incoming requests into outgoing responses. This is a new
feature in Resolver CE v2.2 and later.

In the folder "Example Files to Upload" you will also find an Excel spreadsheet and CSV file with the same data - you
can upload Excel data too! This particular spreadsheet is the 'official GS1 Resolver upload spreadsheet' which is
recognised by the Upload page which sets all the upload columns for you. However, any unencrypted Excel spreadsheet
saved by Excel with extension .xlsx can be read by the upload page.

## Shutting down the service

1. To close the entire application down type this: <pre>docker-compose down</pre> Since the data is stored on Docker
   volumes, the data will survive the shutdown and be available when you 'up' the service again.
1. If you wish to delete the volumes and thus wipe the data, type these commands:

<pre>
docker volume rm gs1_digitallink_resolver_ce_resolver-document-volume
docker volume rm gs1_digitallink_resolver_ce_sql-server-dbbackup-volume
docker volume rm gs1_digitallink_resolver_ce_sql-server-volume-db-data
docker volume rm gs1_digitallink_resolver_ce_sql-server-volume-db-log
docker volume rm gs1_digitallink_resolver_ce_sql-server-volume-db-secrets
</pre>

If the above volumes are the only ones in your Docker Engine then it's quicker to type:<pre>docker volume ls </pre> to
confirm, then to delete all the volumes type:<pre>docker volume prune </pre>

## If you wish to make changes in the any of the service code, run the following commands

1. Ensure that all services are shutdown

<pre> docker-compose down </pre>

2. Pull images associated with the services

<pre> docker-compose pull </pre>

3. Recreate containers even if configuration/image hasn't changed

<pre> docker-compose up --force-recreate --build -d </pre>

<hr />
