import qrcode
import os

def generate_QRcode(Identifier_key_type, Identifier_key):

    gs1_datalink = str(Identifier_key_type) + "/" + str(Identifier_key)

    # write the encrypted data into QRcode
    url = qrcode.make(gs1_datalink)
    # save the QR-code on Desktop folder
    url.save(os.getcwd()+'/QR-codes/' + str(Identifier_key) + '.png')
    # save URL in projects's assets folder to display it on Web application
    print(os.getcwd())
    url.save(os.getcwd()+'/frontend/src/assets/QR_code.png')
    return "Successful"


if __name__ == '__main__':
    # demonstrate encode_into_QRcode method
    generate_QRcode('01','9478231837')