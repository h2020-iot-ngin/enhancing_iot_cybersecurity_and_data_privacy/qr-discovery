import os
import cv2
from pyzbar.pyzbar import decode

def read_QRcode(identifier_key):
    
    # read the QRCODE image from QR-encodings folder
    qr_data = cv2.imread(os.getcwd()+'/QR-codes/' + identifier_key + '.png')
    # initialize the cv2 QRCode detector
    codes = decode(qr_data)
    
    gs1_datalink_bytes = codes[0][0]
    gs1_datalink = gs1_datalink_bytes.decode('utf-8')

    return gs1_datalink


if __name__ == '__main__':
    # demonstrate read_QRcode method
    read_QRcode('00745883713370')