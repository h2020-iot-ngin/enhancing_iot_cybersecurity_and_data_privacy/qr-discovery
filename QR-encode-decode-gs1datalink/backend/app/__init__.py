from flask import Flask, jsonify, request
from flask_cors import CORS

from backend.encode_decode.product_generate import generate_QRcode
from backend.encode_decode.product_reader import read_QRcode


app = Flask(__name__)
# Disable temporary CORS policy to allow front end to access resources at backend
CORS(app, resources={ r'/*': { 'origins': 'http://localhost' } })

# Default route
@app.route('/')
def route_default():
    return 'QR-Encoding app'

# Encode the product details into a QRcode
@app.route('/generate-qrcode', methods=['POST'])
def generate_qrcode_api():

    product_data = request.get_json()
    message = generate_QRcode(product_data['IdentificationKeyType'],\
    product_data['IdentificationKey'])

    if(message == "Successful"):
        return jsonify("QR code generation is successful")
    return jsonify("QR code generation has failed")

# Decode the product details from a QRcode
@app.route('/read-QRcode', methods=['POST'])
def read_qrcode_api():
    identification_key = request.get_json()
    print(identification_key)
    product_temp = read_QRcode(identification_key['IdentificationKey'])

    return jsonify(product_temp)


ROOT_PORT = 5000
PORT = ROOT_PORT

app.run(port=PORT)