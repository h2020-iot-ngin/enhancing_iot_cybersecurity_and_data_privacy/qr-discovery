import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Table, FormGroup, FormControl, FormLabel, Button } from 'react-bootstrap';
import { API_BASE_URL } from '../config';

function CreateEntry() {

  const [Token, setToken] = useState('5555555555555');
  const [IdentificationKeyType, setIdentificationKeyType] = useState('gtin');
  const [IdentificationKey, setIdentificationKey] = useState('--------');
  const [ItemDescription, setItemDescription] = useState('M00420020590');
  const [LinkType, setLinkType] = useState('gs1:pip');
  const [IanaLanguage, setIanaLanguage] = useState('en');
  const [Context, setContext] = useState('xx');
  const [LinkTitle, setLinkTitle] = useState('Product link');
  const [TargetUrl, setTargetUrl] = useState('https://www.google.com');


  var resolver_entries = {
    "identificationKeyType": IdentificationKeyType,
    "identificationKey": IdentificationKey,
    "itemDescription": ItemDescription,
    "dateInserted": "2021-11-17T15:25:07.513Z",
    "dateLastUpdated": "2021-11-17T15:29:41.757Z",
    "qualifierPath": "/",
    "active": true,
    "responses": [
      {
        "linkType": LinkType,
        "ianaLanguage": IanaLanguage,
        "context": Context,
        "mimeType": "text/html",
        "linkTitle": LinkTitle,
        "targetUrl": TargetUrl,
        "defaultLinkType": true,
        "defaultIanaLanguage": true,
        "defaultContext": true,
        "defaultMimeType": true,
        "fwqs": false,
        "active": true
      }
    ]
  }

  const updateToken = event => {
    setToken(event.target.value);
  }
 
  const updateIdentificationKeyType = event => {
    setIdentificationKeyType(event.target.value);
  }

  const updateIdentificationKey = event => {
    setIdentificationKey(event.target.value);
  }

  const updateItemDescription = event => {
    setItemDescription(event.target.value);
  }

  const updateTargetUrl = event => {
    setTargetUrl(event.target.value);
  }

  const updateLinkType = event => {
    setLinkType(event.target.value);
  }

  const updateIanaLanguage = event => {
    setIanaLanguage(event.target.value);
  }

  const updateContext = event => {
    setContext(event.target.value);
  }

  const updateLinkTitle = event => {
    setLinkTitle(event.target.value);
  }

  const CreateEntry = (e) => {
    //e.preventDefault();
    if (resolver_entries["responses"].length !== 0) {
      alert(JSON.stringify(resolver_entries["responses"]))
      fetch(`${API_BASE_URL}/resolver`, {
        mode: 'cors',
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${Token}`, 'Access-Control-Allow-Origin': '*'},
        body: JSON.stringify([resolver_entries])
      }).then(response => response.json())
        .then(json => {
          console.log('CreateEntry json response', JSON.stringify(json));
          alert(JSON.stringify(json));
        });
      }
      else{
        alert("The response fields are empty")
      }
    }

  return (
    <div className="Encoder">
      <br />
      <Link to="/">Back to Home</Link>
      <hr />
      <h3>Create GS1 Digital Link Entry</h3>
      <br />      
      <Table bordered condensed>
        <thead>
          <tr>
            <th>Fields</th>
            <th>Values</th>
          </tr>
        </thead>    
        <tbody>
          <tr>  
            <td><FormLabel>Authentication Key  </FormLabel> {' '}</td>
            <td>
              <FormGroup>        
                <FormControl
                  input="text"
                  placeholder="Authentication Key"
                  value={Token}
                  onChange={updateToken}
                />
              </FormGroup>
            </td>
          </tr>
          <tr>  
            <td><FormLabel>Identification Key Type  </FormLabel> {' '}</td>
            <td>
              <FormGroup>
                <FormControl
                  input="text"
                  placeholder="Identification Key Type"
                  value={IdentificationKeyType}
                  onChange={updateIdentificationKeyType}
                />
              </FormGroup>
            </td>
          </tr>
          <tr>  
            <td><FormLabel>Identification Key  </FormLabel> {' '}</td>
            <td>
              <FormGroup>
                <FormControl
                  input="text"
                  placeholder="Identification Key"
                  value={IdentificationKey}
                  onChange={updateIdentificationKey}
                />
              </FormGroup>
            </td>
          </tr>
          <tr>  
            <td><FormLabel>Item Description  </FormLabel> {' '}</td>
            <td>
              <FormGroup>        
                <FormControl
                  input="text"
                  placeholder="Item Description"
                  value={ItemDescription}
                  onChange={updateItemDescription}
                />
              </FormGroup>
            </td>
          </tr>
          <tr>  
            <td><FormLabel> Link Type  </FormLabel> {' '}</td>
            <td>
              <FormGroup>        
                <FormControl
                  input="text"
                  placeholder="Link Type"
                  value={LinkType}
                  onChange={updateLinkType}
                />
              </FormGroup>
            </td>
          </tr>
          <tr>  
            <td><FormLabel>Iana-Language  </FormLabel> {' '}</td>
            <td>
              <FormGroup>        
                <FormControl
                  input="text"
                  placeholder="Iana-Language"
                  value={IanaLanguage}
                  onChange={updateIanaLanguage}
                />
              </FormGroup>
            </td>
          </tr>
          <tr>  
            <td><FormLabel>Context  </FormLabel> {' '}</td>
            <td>
              <FormGroup>        
                <FormControl
                  input="text"
                  placeholder="Item Description"
                  value={Context}
                  onChange={updateContext}
                />
              </FormGroup>
            </td>
          </tr>
          <tr>  
            <td><FormLabel>Link Title  </FormLabel> {' '}</td>
            <td>
              <FormGroup>        
                <FormControl
                  input="text"
                  placeholder="Item Description"
                  value={LinkTitle}
                  onChange={updateLinkTitle}
                />
              </FormGroup>
            </td>
          </tr>
          <tr>  
            <td><FormLabel>Target Url  </FormLabel> {' '}</td>
            <td>
              <FormGroup>        
                <FormControl
                  input="text"
                  placeholder="Target Url"
                  value={TargetUrl}
                  onChange={updateTargetUrl}
                />
              </FormGroup>
            </td>
          </tr>
        </tbody>
      </Table>
      <div>
        <Button
          variant="primary"
          size = "lg"
          onClick={CreateEntry}
        >
          Submit
        </Button>
      </div>
      <br />
    </div>
  )
  
}

export default CreateEntry;