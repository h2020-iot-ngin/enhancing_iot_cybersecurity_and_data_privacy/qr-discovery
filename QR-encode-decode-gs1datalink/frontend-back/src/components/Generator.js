import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, FormGroup, FormControl, FormLabel, Button } from 'react-bootstrap';
import { API_BASE_URL } from '../config';
import logo from '../assets/QR_code.png';

function Generator() {

  const [IdentificationKeyType, setIdentificationKeyType] = useState('gtin');
  const [IdentificationKey, setIdentificationKey] = useState('00745883713370');

  // fetch previous value of enable submit
  const initialenablecount =  0 || Number(window.localStorage.getItem('enableSubmit')) 
  const [enableSubmit, setenableSubmit] = useState(initialenablecount);

  // read the value of enable submit from local storage
  useEffect(() => {
    window.localStorage.setItem('enableSubmit',enableSubmit);
  },[enableSubmit]);
  
  // enable submit button
  const EnableSubmit = (input) => {
    setenableSubmit(Number(input));
    window.localStorage.setItem('enableSubmit',enableSubmit);
  }
  
  const updateIdentificationKeyType = event => {
    setIdentificationKeyType(event.target.value);
  }

  const updateIdentificationKey = event => {
    setIdentificationKey(event.target.value);
  }

  const Generator = () => {
    fetch(`${API_BASE_URL}:5000/generate-qrcode`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ IdentificationKeyType, IdentificationKey})
    }).then(response => response.json())
      .then(json => {
        console.log('Generator json', json);
        alert('Successfully encoded gs1 data link into QR code!');
      });
  }

  if (enableSubmit == 1) {
    return (
      <div className="Generator">
      <br />
      <Link to="/">Back to Home</Link>
      <hr />
      <div><img className="logo" src={logo} alt="application-logo" /></div>
      <div>
        <br />
        <Button
          variant="danger"
          onClick={ () => {
            EnableSubmit(0);
          }}
        >
          Back
        </Button>
      </div>
      <br />
    </div>
    )
  }
  return (
    <div className="Generator">
      <br />
      <Link to="/">Back to Home</Link>
      <hr />
      <h3>Generate QR-code for GS1 Digital Link </h3>
      <br />      
      <Table bordered condensed>
        <thead>
          <tr>
            <th>Fields</th>
            <th>Values</th>
          </tr>
        </thead>    
        <tbody>
          <tr>  
            <td><FormLabel>Enter Identification Key Type</FormLabel> {' '}</td>
            <td>
              <FormGroup>        
                <FormControl
                  input="text"
                  placeholder="Enter Identification Key Type"
                  value={IdentificationKeyType}
                  onChange={updateIdentificationKeyType}
                />
              </FormGroup>
            </td>
          </tr>
          <tr>  
            <td><FormLabel>Enter Identification Key</FormLabel> {' '}</td>
            <td>
              <FormGroup>        
                <FormControl
                  input="text"
                  placeholder="Enter Identification Key"
                  value={IdentificationKey}
                  onChange={updateIdentificationKey}
                />
              </FormGroup>{' '}
            </td>
          </tr>
        </tbody>
      </Table>
      <div>
        <Button
          variant="primary"
          onClick={ () => {
            Generator();
            EnableSubmit(1);
          }}
        >
          Generate QR code for the GS1 data link
        </Button>
      </div>
      <br />
    </div>
  )
  
}

export default Generator;