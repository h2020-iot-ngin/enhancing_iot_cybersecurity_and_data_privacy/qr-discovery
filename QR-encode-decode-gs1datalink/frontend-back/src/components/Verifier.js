import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Table, FormGroup, FormControl, FormLabel, Button } from 'react-bootstrap';
import { API_BASE_URL } from '../config';

function Verifier() {
  const [IdentificationKey, setIdentificationKey] = useState('00745883713370');
  const [Token, setToken] = useState('5555555555555');
  
  const [ResponseList, setResponseList] = useState([]);

  const updateIdentificationKey = event => {
    setIdentificationKey(event.target.value);
  }

  const Submit = () => {
    fetch(`${API_BASE_URL}:5000/read-QRcode`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ IdentificationKey })
    }).then(response => response.json())
      .then(json => {
        console.log('Verification Complete - json', json);
        alert(json)
        fetch(`${API_BASE_URL}/resolver/${json}`, {
          mode: 'cors',
          method: 'GET',
          headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${Token}`, 'Access-Control-Allow-Origin': '*'}
        }).then(response => response.json())
        .then(json => {
          console.log('GS1 Datalink Data - json', json);
          setResponseList(json['data'][0]["responses"])
          alert(json['data'][0]["responses"][0]["targetUrl"]);
        });
  
      });
  }

  return (
    <div className="Verifier">
      <Link to="/">Back to Home</Link>
      <hr />
      <h3>Decode QR code</h3>
      <br />
      <Table bordered condensed>
        <thead>
          <tr>
            <th>Fields</th>
            <th>Values</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><FormLabel> Identification Key </FormLabel> {' '}</td>
            <td>
              <FormGroup>
                <FormControl
                  input="text"
                  placeholder="Enter Identification Key"
                  value={IdentificationKey}
                  onChange={updateIdentificationKey}
                />
              </FormGroup>{' '}
            </td>
          </tr>
        </tbody>
      </Table>
      <div>
        <Button
          variant="danger"
          onClick={Submit}
        >
          Decode QRcode
        </Button>
      </div>
      <br />
      <Table bordered responsive>
        <thead>
          <tr>
              <th rowSpan="2">linkType</th>
              <th rowSpan="2">ianaLanguage</th>
              <th rowSpan="2">context</th>
              <th rowSpan="2">linkTitle</th>
              <th rowSpan="2">targetUrl</th>
          </tr>
          <hr />
        </thead>
        <tbody>
        {
          ResponseList.map((Response) => (
            <tr>
              <td>{Response["linkType"]} </td>
              <td>{Response["ianaLanguage"]} </td>
              <td>{Response["context"]} </td>
              <td>{Response["linkTitle"]} </td>
              <td>{Response["targetUrl"]} </td>
            </tr>
          ))
        }
        </tbody>
      </Table>
      <br />
    </div>
  )
}

export default Verifier;