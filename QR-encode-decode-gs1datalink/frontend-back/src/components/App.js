import React from 'react';
import { Link } from 'react-router-dom';
import frontlogo from '../assets/spanner.png';
import { Table, FormGroup, FormLabel } from 'react-bootstrap';

function App() {

  return (
    <div className="App">
      
      <h2>QR encoded GS1 Digital Link Resolver</h2>
      <div><center><img className="frontlogo" src={frontlogo} alt="application-logo" /></center></div>
      <br />
      <hr />
      <Table bordered condensed>
        <thead>
          <tr>
            <th>Operations</th>
          </tr>
        </thead>    
        <tbody>
        <tr>  
            <td>
              <FormGroup>
                <Link to="/CreateEntry"><h4>Create GS1 datalink Entry</h4></Link>
              </FormGroup>{' '}
            </td>
          </tr>
          <tr>  
            <td>
              <FormGroup>
              <Link to="/Generator"><h4> QR-code Generator</h4></Link>
              </FormGroup>{' '}
            </td>
          </tr>
          <tr>  
            <td>
              <FormGroup>
                <Link to="/Verifier"><h4> QR-code Verifier</h4></Link>
              </FormGroup>{' '}
            </td>
          </tr>
        </tbody>
      </Table>
      <Link to="/in-development"><h4>Gitlab SDK</h4></Link>
      <br/>
    </div>
  );
}

export default App;