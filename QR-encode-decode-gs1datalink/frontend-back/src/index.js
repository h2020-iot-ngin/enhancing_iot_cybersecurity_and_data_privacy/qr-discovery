import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Switch, Route } from 'react-router-dom';
import './index.css';
import history from './history';
import App from './components/App';
import Generator from './components/Generator';
import Verifier from './components/Verifier';
import CreateEntry from './components/CreateEntry';
ReactDOM.render(
  <Router history={history}>
    <Switch>
      <Route path='/' exact component={App} />
      <Route path='/Generator' component={Generator} />
      <Route path='/Verifier' component={Verifier} />
      <Route path='/CreateEntry' component={CreateEntry} />
    </Switch>
  </Router>,
  document.getElementById('root')
);