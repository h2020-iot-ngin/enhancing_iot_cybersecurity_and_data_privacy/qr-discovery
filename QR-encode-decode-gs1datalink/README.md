# QR Encoding and Decoding for GS1 data link <a name="TOP"></a>

## Usage

![QR-encoding.png](QR-encoding.png)

## Prerequisites

    * python3 (3.8.10)
    * Flask
    * Flask-Cors
    * qrcode
    * opencv-python
    * pyzbar

## Installation steps

This project is implemented on **Ubuntu-20.04 OS**.

### Step 1: Go to the folder that you want to install the project.

### Step 2: Set up the virtual environment

```
sudo apt-get update
sudo apt-get install python3-venv
python3 -m venv QR-encoding-env
```

### Step 3: Activate the virtual environment

```
source QR-encoding-env/bin/activate
```

### Step 4: Check if the virtual environment is properly set

```
echo $VIRTUAL_ENV
```

### Step 5: Install pip3

```
sudo apt-get install python3-pip
```

### Step 6.a: Install all the dependencies required for the project

```
pip3 install -r requirements.txt
```

### Step 6.b: Check the following commands if you wish to install individual dependencies. (Optional)

```
pip3 install Flask==1.1.1
pip3 install Flask-Cors==3.0.8
pip3 install qrcode==7.3.1
pip3 install opencv-python==4.5.3.56
pip3 install pyzbar==0.1.8
```

### Step 7: Install npm

```
sudo apt-get install npm
```

### Step 8: Check npm and npx are installed properly

```
npm -v
npx -v
```

### Step 9: Install firefox addon to allow the user to enable CORS everywhere by altering http responses.

```
https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
```

### Step 10: Create ReactJS frontend folder

```
npx create-react-app frontend
```

### Step 11: Copy the _src_ and _public_ folders from **frontend-back** folder to **frontend** folder

### Step 12: cd to the _frontend_ folder

```
cd frontend
```

### Step 13: Install following npm dependencies

```
npm i react-bootstrap --save
npm i react-router@5.0.1 react-router-dom@5.0.1 history@4.9.0 --save
npm install react-bootstrap-table-next --save
npm install react-bootstrap-table2-filter --save
```

### Step 14: start frontend server

```
npm run start
```

### Step 15: open another terminal and cd to the project folder

### Step 16: Make sure you have activated the virtual enironment

```
source QR-encoding-env/bin/activate
```

### Step 17: Start the backend application

```
python3 -m backend.app
```
